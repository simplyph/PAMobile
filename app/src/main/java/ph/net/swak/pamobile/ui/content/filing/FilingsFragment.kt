package ph.net.swak.pamobile.ui.content.filing


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_filings.view.*
import ph.net.swak.pamobile.R
import ph.net.swak.pamobile.ui.base.BaseFragment

class FilingsFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_filings, container, false)
        view.pager.adapter = FilingsSectionsPagerAdapter(childFragmentManager)
        view.tabLayout.setupWithViewPager(view.pager)
        return view
    }

}

package ph.net.swak.pamobile.ui.content.filing

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import ph.net.swak.pamobile.ui.content.filing.itinerary.ItineraryFragment
import ph.net.swak.pamobile.ui.content.filing.leave.LeaveFragment

class FilingsSectionsPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    private var tabTitles: Array<String> = arrayOf("Itinerary", "Leave")

    override fun getCount(): Int = tabTitles.size

    override fun getItem(position: Int): Fragment = when (position) {
        0 -> ItineraryFragment()
        else -> LeaveFragment()
    }

    override fun getItemPosition(`object`: Any): Int = PagerAdapter.POSITION_NONE

    override fun getPageTitle(position: Int): CharSequence = tabTitles[position]

}
package ph.net.swak.pamobile.di.module.sub

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ph.net.swak.pamobile.di.annotation.scope.ApplicationScope
import javax.inject.Named

@Module
class ThreadingModule {

    companion object {
        const val MAIN_SCHEDULER = "main_scheduler"
        const val BACKGROUND_SCHEDULER = "background_scheduler"
    }

    @Provides
    @ApplicationScope
    @Named(MAIN_SCHEDULER)
    fun provideMainScheduler(): Scheduler = AndroidSchedulers.mainThread()

    @Provides
    @ApplicationScope
    @Named(BACKGROUND_SCHEDULER)
    fun provideBackgroundScheduler(): Scheduler = Schedulers.io()

}
package ph.net.swak.pamobile.ui.content.login

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import ph.net.swak.domain.model.Login
import ph.net.swak.domain.util.Status
import ph.net.swak.pamobile.R
import ph.net.swak.pamobile.ui.base.BaseActivity
import ph.net.swak.pamobile.ui.base.PAMobileViewModelFactory
import ph.net.swak.pamobile.ui.content.home.HomeActivity
import javax.inject.Inject

class LoginActivity : BaseActivity() {

    @Inject lateinit var viewModelFactory: PAMobileViewModelFactory
    @Inject lateinit var sharedPrefs: SharedPreferences

    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (sharedPrefs.getBoolean("login", false)) {
            startActivity<HomeActivity>()
            finish()
        }

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)
        setupListeners()
        setupControls()
    }

    private fun setupListeners() {
        viewModel.login.observe(this, Observer {
            it?.let {
                when (it.status) {
                    Status.SUCCESS -> {
                        it.data?.let {
                            saveLoginDetails(it)
                            startActivity<HomeActivity>()
                            finish()
                        }
                    }

                    Status.ERROR -> {
                        toast(it.message ?: getString(R.string.default_error_message))
                        showProgress(false)
                    }

                    Status.LOADING -> showProgress(true)

                    else -> {
                    }
                }
            }
        })
    }

    private fun setupControls() {
        password_layout.isPasswordVisibilityToggleEnabled = true

        addDisposables(login.clicks().subscribeBy(onNext = { attemptLogin() }))
    }

    private fun attemptLogin() {
        email.error = null
        password.error = null

        val emailStr = email.text.toString()
        val passwordStr = password.text.toString()

        var cancel = false
        var focusView: View? = null

        if (passwordStr.isBlank()) {
            password.error = getString(R.string.error_field_required)
            focusView = password
            cancel = true
        }

        if (emailStr.isBlank()) {
            email.error = getString(R.string.error_field_required)
            focusView = email
            cancel = true
        }

        if (cancel) {
            focusView?.requestFocus()
        } else {
            viewModel.checkLogin(emailStr, passwordStr)
        }
    }

    private fun showProgress(show: Boolean) {
        val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime).toLong()

        login_form.visibility = if (show) View.GONE else View.VISIBLE
        login_form.animate()
                .setDuration(shortAnimTime)
                .alpha((if (show) 0 else 1).toFloat())
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        login_form.visibility = if (show) View.GONE else View.VISIBLE
                    }
                })

        login_progress.visibility = if (show) View.VISIBLE else View.GONE
        login_progress.animate()
                .setDuration(shortAnimTime)
                .alpha((if (show) 1 else 0).toFloat())
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        login_progress.visibility = if (show) View.VISIBLE else View.GONE
                    }
                })

    }

    private fun saveLoginDetails(login: Login) {
        sharedPrefs.edit()
                .putBoolean("login", true)
                .putBoolean("first_login", true)
                .putString("account_role", login.accountName)
                .putString("account_img", login.accountImage)
                .putInt("account_type", login.accountType)
                .putString("name", login.employeeName)
                .putString("username", email.text.toString())
                .putString("password", password.text.toString())
                .apply()
    }
}

package ph.net.swak.domain.util

enum class Status {
    IDLE,
    SUCCESS,
    LOADING,
    ERROR
}
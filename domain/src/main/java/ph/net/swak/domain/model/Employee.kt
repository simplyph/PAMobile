package ph.net.swak.domain.model

data class Employee(
        val id: Int,
        val firstName: String,
        val middleName: String,
        val lastName: String
)
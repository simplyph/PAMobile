package ph.net.swak.pamobile.ui.animation.transition

interface OnRevealAnimationListener {

    fun onRevealHide()
    fun onRevealShow()

}
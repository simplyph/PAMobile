package ph.net.swak.domain.interactor.employee

import ph.net.swak.domain.interactor.type.CompletableUseCaseWithParameter
import ph.net.swak.domain.repository.EmployeeRepository

class GetLeaveUseCase(private val repo: EmployeeRepository) : CompletableUseCaseWithParameter<Int> {

    override fun execute(parameter: Int) = repo.getLeave(parameter)

}
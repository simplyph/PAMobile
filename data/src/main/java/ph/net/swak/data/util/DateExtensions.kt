package ph.net.swak.data.util

import java.text.SimpleDateFormat
import java.util.*

val locale: Locale = Locale.US

fun Date.to12HourTime(): String = SimpleDateFormat("KK:mm aa", locale).format(this)

fun Date.to24HourTime(): String = SimpleDateFormat("HH:mm aa", locale).format(this)

fun Date.toWholeDate(): String = SimpleDateFormat("MMM dd, yyyy", locale).format(this)

fun Date.toNumberDate(): String = SimpleDateFormat("MM/dd/yyyy", locale).format(this)

fun Date.toDatabaseDate(): String = SimpleDateFormat("yyyy-MM-dd", locale).format(this)

fun Date.isCurrentDate(): Boolean {
    val df = SimpleDateFormat("yyyy-MM-dd", locale)

    return df.format(Calendar.getInstance().time) == df.format(this)
}

fun Date.toYear(): String = SimpleDateFormat("yyyy", locale).format(this)

fun parseYear(value: String) = parseDate(value)
        .toYear()
        .toInt()

fun parseDate(value: String): Date {
    return if(value.trim().isNotEmpty()) {
        SimpleDateFormat("M/dd/yyyy K:mm:ss aa", locale).parse(value)
    } else {
        Date(0)
    }
}



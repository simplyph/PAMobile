package ph.net.swak.domain.model

import java.util.*

data class Itinerary (
        val isApproved: Int,
        val dateFiled: Date,
        val dateCreated: Date,
        val reason: String,
        val createdBy: String,
        val dateFrom: Date,
        val dateTo: Date,
        val disapprovedReason: String,
        val appDisBy: String,
        val dateAppDis: Date,
        val returnMessage: String
)
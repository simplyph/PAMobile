package ph.net.swak.data.service.model

data class ApiEmployee(
        val id: Int,
        val firstName: String,
        val middleName: String,
        val lastName: String
)
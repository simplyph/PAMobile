package ph.net.swak.pamobile.ui.content.filing.leave

import android.arch.persistence.room.util.StringUtil
import android.support.v4.text.TextUtilsCompat
import android.text.TextUtils
import android.view.View
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.commons.utils.FastAdapterUIUtils
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.materialize.holder.StringHolder
import com.mikepenz.materialize.util.UIUtils
import kotlinx.android.synthetic.main.item_leave.view.*
import ph.net.swak.data.util.isCurrentDate
import ph.net.swak.data.util.toNumberDate
import ph.net.swak.data.util.toWholeDate
import ph.net.swak.domain.util.toTitleCase
import ph.net.swak.pamobile.R
import ph.net.swak.pamobile.util.ApproveStatusText
import java.util.*

class LeaveItem(
        private val status: Int,
        private val approval: String,
        private val dateFiled: Date,
        private val dateLeave: Date,
        private val leaveDays: Float,
        private val leaveDaysApproved: Float,
        private val isPaid: Int,
        private val leaveName: String,
        private val reason: String
) : AbstractItem<LeaveItem, LeaveItem.ViewHolder>() {

    override fun getViewHolder(v: View) = ViewHolder(v)

    override fun getLayoutRes() = R.layout.item_leave

    override fun getType() = R.id.fast_adapter_list_leave_id

    inner class ViewHolder(private val view: View) : FastAdapter.ViewHolder<LeaveItem>(view) {

        override fun bindView(item: LeaveItem?, payloads: MutableList<Any>?) {
            item?.let {
                val ctx = view.context

                UIUtils.setBackground(view, FastAdapterUIUtils.getSelectableBackground(ctx))

                val statusText =
                        if (ApproveStatusText(it.status).toLowerCase() == "approved")
                            "${ApproveStatusText(it.status)} by ${it.approval}"
                        else
                            ApproveStatusText(it.status)

                val filedText = if (it.dateFiled.isCurrentDate()) "Today" else it.dateFiled.toNumberDate()

                val leaveDaysText =
                        if (ApproveStatusText(it.status).toLowerCase() == "approved")
                            it.leaveDaysApproved
                        else
                            it.leaveDays

                val dateText = "${it.dateLeave.toWholeDate()} - ${if(leaveDaysText >= 1) "Whole day" else "Half day"}"

                val typeText = "${if(it.isPaid == 1) "Paid" else "Unpaid"} ${it.leaveName.toTitleCase()}"

                StringHolder.applyTo(StringHolder(statusText), view.tv_status)
                StringHolder.applyTo(StringHolder(filedText), view.tv_filed)
                StringHolder.applyTo(StringHolder(dateText), view.tv_date)
                StringHolder.applyTo(StringHolder(typeText), view.tv_type)
                StringHolder.applyTo(StringHolder(it.reason), view.tv_reason)
            }
        }

        override fun unbindView(item: LeaveItem?) {
            view.tv_status.text = null
            view.tv_filed.text = null
            view.tv_date.text = null
            view.tv_type.text = null
            view.tv_reason.text = null
        }

    }

}
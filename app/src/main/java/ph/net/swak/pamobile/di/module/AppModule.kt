package ph.net.swak.pamobile.di.module

import android.app.Application
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.apollographql.apollo.ApolloClient
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import dagger.Module
import dagger.Provides
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import ph.net.swak.pamobile.R
import ph.net.swak.pamobile.di.module.sub.*
import ph.net.swak.pamobile.di.annotation.scope.ApplicationScope
import ph.net.swak.pamobile.network.TokenAuthenticator
import ph.net.swak.pamobile.util.BASE_URL
import ph.net.swak.pamobile.util.GRAPHQL_ENDPOINT
import java.io.IOException

@Module(includes = [(ServiceModule::class), (DataModule::class), (ThreadingModule::class), (UseCaseModule::class), (ViewModelModule::class)])
class AppModule {

    @Provides
    @ApplicationScope
    fun provideContext(application: Application): Context = application.applicationContext

    @Provides
    @ApplicationScope
    fun provideTokenAuthenticator(application: Application): TokenAuthenticator =
            TokenAuthenticator(application.applicationContext)

    @Provides
    @ApplicationScope
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor =
            HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

    @Provides
    @ApplicationScope
    fun provideOKHttpClient(tokenAuthenticator: TokenAuthenticator,
                            logging: HttpLoggingInterceptor): OkHttpClient {
        val dispatcher = Dispatcher()
        dispatcher.maxRequests = 1

        return OkHttpClient.Builder()
                .authenticator(tokenAuthenticator)
                .dispatcher(dispatcher)
                .addInterceptor(logging)
                .addNetworkInterceptor(logging)
                .build()
    }

    @Provides
    @ApplicationScope
    fun provideApolloClient(okHttpClient: OkHttpClient): ApolloClient =
            ApolloClient.builder()
                    .serverUrl("$BASE_URL$GRAPHQL_ENDPOINT")
                    .okHttpClient(okHttpClient)
                    .build()

    @Provides
    @ApplicationScope
    fun provideGson(): Gson =
            GsonBuilder().setDateFormat("MMM dd, YYYY")
                    .registerTypeAdapter(Boolean::class.java, booleanAsIntAdapter)
                    .create()

    @Provides
    @ApplicationScope
    fun provideSharedPreferences(context: Context): SharedPreferences =
            context.getSharedPreferences(context.getString(R.string.preference_file_key), MODE_PRIVATE)

    companion object {
        private val booleanAsIntAdapter = object : TypeAdapter<Boolean>() {
            @Throws(IOException::class)
            override fun write(out: JsonWriter, value: Boolean?) {
                if (value == null) {
                    out.nullValue()
                } else {
                    out.value(value)
                }
            }

            @Throws(IOException::class)
            override fun read(jsonReader: JsonReader): Boolean? = when (jsonReader.peek()) {

                JsonToken.BOOLEAN -> jsonReader.nextBoolean()

                JsonToken.NULL -> {
                    jsonReader.nextNull()
                    null
                }

                JsonToken.NUMBER -> jsonReader.nextInt() != 0

                JsonToken.STRING -> jsonReader.nextString().equals("1", ignoreCase = true)

                else -> throw IllegalStateException("Expected BOOLEAN or NUMBER " +
                        "but was ${jsonReader.peek()}")
            }
        }
    }
}
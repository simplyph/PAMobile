package ph.net.swak.domain.model

data class LoginCredential(
        val user: String,
        val pass: String
)
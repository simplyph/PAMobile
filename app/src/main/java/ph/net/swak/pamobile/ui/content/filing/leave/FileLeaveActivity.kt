package ph.net.swak.pamobile.ui.content.filing.leave

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_file_leave.*
import ph.net.swak.pamobile.R
import ph.net.swak.pamobile.ui.base.BaseActivity

class FileLeaveActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_file_leave)
        setSupportActionBar(toolbar)
        supportActionBar?.setTitle(R.string.title_file_leave)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportPostponeEnterTransition()

        initViews()
    }

    private fun initViews() {
        Handler(Looper.getMainLooper()).post {
            val animation = AnimationUtils.loadAnimation(this, android.R.anim.fade_in)
            animation.duration = 300
            file_form.startAnimation(animation)
            file_form.visibility = View.VISIBLE
            supportStartPostponedEnterTransition()
        }
    }
}
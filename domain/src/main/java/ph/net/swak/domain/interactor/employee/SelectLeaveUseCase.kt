package ph.net.swak.domain.interactor.employee

import io.reactivex.Flowable
import ph.net.swak.domain.interactor.type.FlowableUseCaseWithParameter
import ph.net.swak.domain.model.Leave
import ph.net.swak.domain.repository.EmployeeRepository

class SelectLeaveUseCase(private val repo: EmployeeRepository) : FlowableUseCaseWithParameter<Int, List<Leave>> {

    override fun execute(parameter: Int): Flowable<List<Leave>> = repo.selectLeave(parameter)

}
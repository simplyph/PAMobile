package ph.net.swak.data.service

import android.util.Log
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.exceptions.Exceptions
import ph.net.swak.data.*
import ph.net.swak.data.db.dao.ItineraryDao
import ph.net.swak.data.db.dao.LeaveDao
import ph.net.swak.data.util.parseDate
import ph.net.swak.data.util.toDb
import ph.net.swak.data.util.toDomain
import ph.net.swak.domain.model.*

class EmployeeService(
        private val apolloClient: ApolloClient,
        private val itineraryDao: ItineraryDao,
        private val leaveDao: LeaveDao
) : IEmployeeService {

    override fun fetchEmployees(): Single<List<Employee>> {
        Log.e("SampleTag", "EmployeeService")

        val query = GetEmployeeCompleteQuery.builder()
                .id(17001020)
                .build()
        val apolloCall = apolloClient.query(query)

        return Single.defer {
            Single.create<List<Employee>> { emitter ->
                apolloCall.enqueue(object : ApolloCall.Callback<GetEmployeeCompleteQuery.Data>() {

                    override fun onFailure(e: ApolloException) {
                        Exceptions.throwIfFatal(e)
                        if (!emitter.isDisposed)
                            emitter.onError(e)
                    }

                    override fun onResponse(response: Response<GetEmployeeCompleteQuery.Data>) {
                        if (!emitter.isDisposed) {

                            val ret: MutableList<Employee> = mutableListOf()
                            emitter.onSuccess(ret)
                        }
                    }
                })
            }
        }
    }

    override fun fetchItinerary(id: Int): Single<List<Itinerary>> {
        val query = GetItineraryQuery.builder()
                .id(id)
                .build()

        val apolloCall = apolloClient.query(query)

        return Single.defer {
            Single.create<List<Itinerary>> { emitter ->
                apolloCall.enqueue(object : ApolloCall.Callback<GetItineraryQuery.Data>() {

                    override fun onFailure(e: ApolloException) {
                        Exceptions.throwIfFatal(e)

                        if (!emitter.isDisposed)
                            emitter.onError(e)
                    }

                    override fun onResponse(response: Response<GetItineraryQuery.Data>) {
                        if (!emitter.isDisposed) {
                            val ret: MutableList<Itinerary> = mutableListOf()
                            response.data()?.let {
                                it.itinerary()?.let {
                                    it.items()?.let {
                                        it.mapTo(ret) {
                                            Itinerary(
                                                    it.isApproved,
                                                    parseDate(it.dateFiled()),
                                                    parseDate(it.dateCreated()),
                                                    it.reason(),
                                                    it.createdBy(),
                                                    parseDate(it.dateFrom()),
                                                    parseDate(it.dateTo()),
                                                    it.disapprovedReason(),
                                                    it.appDisBy(),
                                                    parseDate(it.appDisDate()),
                                                    it.returnMessage()
                                            )
                                        }
                                    }
                                }
                            }

                            emitter.onSuccess(ret)
                        }
                    }
                })
            }
        }
    }

    override fun getItinerary(id: Int): Completable {
        val query = GetItineraryQuery.builder()
                .id(id)
                .build()

        val apolloCall = apolloClient.query(query)

        return Completable.defer {
            Completable.create { emitter ->
                apolloCall.enqueue(object : ApolloCall.Callback<GetItineraryQuery.Data>() {

                    override fun onFailure(e: ApolloException) {
                        Exceptions.throwIfFatal(e)

                        if (!emitter.isDisposed)
                            emitter.onError(e)
                    }

                    override fun onResponse(response: Response<GetItineraryQuery.Data>) {
                        if (!emitter.isDisposed) {
                            response.data()?.let {
                                it.itinerary()?.let {
                                    it.items()?.let {
                                        itineraryDao.insert(it.toDb())
                                    }
                                }
                            }
                            emitter.onComplete()
                        }
                    }
                })
            }
        }
    }

    override fun postItinerary(fileItinerary: FileItinerary): Completable {
        val mutation = PostItineraryMutation.builder()
                .id(fileItinerary.id)
                .year(fileItinerary.year)
                .fromDate(fileItinerary.fromDate)
                .toDate(fileItinerary.toDate)
                .reason(fileItinerary.reason)
                .build()

        val apolloCall = apolloClient.mutate(mutation)

        return Completable.defer {
            Completable.create { emitter ->
                apolloCall.enqueue(object : ApolloCall.Callback<PostItineraryMutation.Data>() {

                    override fun onResponse(response: Response<PostItineraryMutation.Data>) {
                        if (!emitter.isDisposed)
                            response.data()?.let {
                                it.createItinerary()?.let {
                                    if (it.returnMessage().toLowerCase() == "success") {
                                        emitter.onComplete()
                                    } else {
                                        emitter.onError(Throwable("Mutation didn't succeed"))
                                    }
                                }
                            }
                    }

                    override fun onFailure(e: ApolloException) {
                        Exceptions.throwIfFatal(e)

                        if (!emitter.isDisposed)
                            emitter.onError(e)
                    }
                })
            }
        }
    }

    override fun getLogin(credential: LoginCredential): Single<Login> {
        val query = GetLoginQuery.builder()
                .user(credential.user)
                .pass(credential.pass)
                .build()

        val apolloCall = apolloClient.query(query)

        return Single.defer {
            Single.create<Login> { emitter ->
                apolloCall.enqueue(object : ApolloCall.Callback<GetLoginQuery.Data>() {

                    override fun onResponse(response: Response<GetLoginQuery.Data>) {
                        if (!emitter.isDisposed)
                            response.data()?.let {
                                it.login()?.let {
                                    if (it.isValid)
                                        if (it.isActive)
                                            emitter.onSuccess(it.toDomain())
                                        else
                                            emitter.onError(Throwable("The account is inactive!"))
                                    else
                                        emitter.onError(Throwable("Invalid Login Details!"))
                                }
                            }
                    }

                    override fun onFailure(e: ApolloException) {
                        Exceptions.throwIfFatal(e)

                        if (!emitter.isDisposed)
                            emitter.onError(e)
                    }

                })
            }
        }

    }

    override fun getLeave(id: Int): Completable {
        val query = GetLeaveQuery.builder()
                .id(id)
                .build()

        val apolloCall = apolloClient.query(query)

        return Completable.defer {
            Completable.create { emitter ->
                apolloCall.enqueue(object : ApolloCall.Callback<GetLeaveQuery.Data>() {

                    override fun onResponse(response: Response<GetLeaveQuery.Data>) {
                        if (!emitter.isDisposed) {
                            response.data()?.let {
                                it.leave()?.let {
                                    it.items()?.let {
                                        leaveDao.insert(it.toDb())
                                    }
                                }
                            }
                            emitter.onComplete()
                        }
                    }

                    override fun onFailure(e: ApolloException) {
                        Exceptions.throwIfFatal(e)

                        if (!emitter.isDisposed)
                            emitter.onError(e)
                    }
                })
            }
        }
    }
}
package ph.net.swak.data.mapper

import ph.net.swak.data.GetLeaveQuery
import ph.net.swak.data.db.entities.LeaveEntity
import ph.net.swak.data.mapper.base.ApiToDbMapper
import ph.net.swak.data.util.parseDate
import ph.net.swak.data.util.parseYear
import ph.net.swak.domain.model.Leave


class LeaveMapper : ApiToDbMapper<List<GetLeaveQuery.Item>, List<LeaveEntity>, List<Leave>> {

    override fun apiToDb(apiModel: List<GetLeaveQuery.Item>): List<LeaveEntity> =
            apiModel.map {
                LeaveEntity(
                        it.id().toLong(),
                        it.empId(),
                        it.isApproved,
                        parseDate(it.dateFiled()),
                        parseDate(it.dateCreated()),
                        it.reason(),
                        it.createdBy(),
                        parseYear(it.dateFrom()),
                        parseDate(it.dateFrom()),
                        parseDate(it.dateTo()),
                        it.disapprovedReason(),
                        it.appDisBy(),
                        parseDate(it.appDisDate()),
                        it.leaveName(),
                        it.leaveId(),
                        it.leaveBalance().toFloat(),
                        parseDate(it.dateLeave()),
                        it.leaveDaysFiled().toFloat(),
                        it.leaveDaysApproved().toFloat(),
                        it.isPaid,
                        it.leaveStatus()
                )
            }

    override fun dbToDomain(entity: List<LeaveEntity>): List<Leave> =
            entity.map {
                Leave(
                        it.isApproved,
                        it.dateFiled,
                        it.dateCreated,
                        it.reason,
                        it.createdBy,
                        it.dateFrom,
                        it.dateTo,
                        it.disapprovedReason,
                        it.appDisBy,
                        it.appDisDate,
                        it.leaveName,
                        it.leaveId,
                        it.leaveBalance,
                        it.dateLeave,
                        it.daysFiled,
                        it.daysApproved,
                        it.isPaid,
                        it.leaveStatus,
                        "Success"
                )
            }
}
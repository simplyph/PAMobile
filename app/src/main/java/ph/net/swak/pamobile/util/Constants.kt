package ph.net.swak.pamobile.util

const val LOCAL_URL = "http://localhost:3231/"
const val NETWORK_URL = "http://mdrpayroll.swak.net.ph/"
const val BASE_URL = NETWORK_URL
const val TOKEN_ENDPOINT = "token"
const val GRAPHQL_ENDPOINT = "api/graphql"
const val CLIENT_TYPE = "1"
const val CLIENT_ID = "Android"
const val CLIENT_SECRET = "swakAndroid_webApi_2017"


const val REQUEST_CODE_FILE_ITINERARY = 1400
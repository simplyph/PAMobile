package ph.net.swak.domain.util

data class Resource<out T>(
        val status: Status,
        val message: String?,
        val data: T?
)
package ph.net.swak.data.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import ph.net.swak.data.db.entities.ItineraryEntity

@Dao
interface ItineraryDao {

    @Query("SELECT * FROM itineraries WHERE emp_id = :empId ORDER BY date_from DESC")
    fun getItinerary(empId: Int): Flowable<List<ItineraryEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(itinerary: List<ItineraryEntity>)

}
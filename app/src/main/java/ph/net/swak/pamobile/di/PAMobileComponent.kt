package ph.net.swak.pamobile.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import ph.net.swak.pamobile.PAMobileApplication
import ph.net.swak.pamobile.di.module.ActivityModule
import ph.net.swak.pamobile.di.module.AppModule
import ph.net.swak.pamobile.di.annotation.scope.ApplicationScope

@ApplicationScope
@Component(modules = [(AndroidSupportInjectionModule::class), (AppModule::class), (ActivityModule::class)])
interface PAMobileComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): PAMobileComponent
    }

    fun inject(app: PAMobileApplication)
}
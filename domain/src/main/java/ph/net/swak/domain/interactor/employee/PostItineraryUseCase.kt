package ph.net.swak.domain.interactor.employee

import io.reactivex.Completable
import ph.net.swak.domain.interactor.type.CompletableUseCaseWithParameter
import ph.net.swak.domain.model.FileItinerary
import ph.net.swak.domain.repository.EmployeeRepository

class PostItineraryUseCase(private val repo: EmployeeRepository) : CompletableUseCaseWithParameter<FileItinerary> {

    override fun execute(parameter: FileItinerary) = repo.postItinerary(parameter)
}
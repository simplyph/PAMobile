package ph.net.swak.domain.model

data class FileItinerary(
        val id: Int,
        val year: Int,
        val fromDate: String,
        val toDate: String,
        val reason: String
)
package ph.net.swak.domain.interactor.employee

import io.reactivex.Single
import ph.net.swak.domain.interactor.type.SingleUseCaseWithParameter
import ph.net.swak.domain.model.Login
import ph.net.swak.domain.model.LoginCredential
import ph.net.swak.domain.repository.EmployeeRepository

class GetLoginUseCase(private val repo: EmployeeRepository) : SingleUseCaseWithParameter<LoginCredential, Login> {

    override fun execute(parameter: LoginCredential): Single<Login> = repo.getLogin(parameter)

}
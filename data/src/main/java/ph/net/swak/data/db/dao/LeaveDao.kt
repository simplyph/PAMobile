package ph.net.swak.data.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import ph.net.swak.data.db.entities.LeaveEntity

@Dao
interface LeaveDao {

    @Query("SELECT * FROM leaves WHERE emp_id = :empId ORDER BY leave_date DESC")
    fun getLeave(empId: Int): Flowable<List<LeaveEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(leave: List<LeaveEntity>)

}
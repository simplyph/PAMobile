package ph.net.swak.domain.interactor.type

import io.reactivex.Completable

interface CompletableUseCase {

    fun execute(): Completable

}
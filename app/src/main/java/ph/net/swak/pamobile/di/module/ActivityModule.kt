package ph.net.swak.pamobile.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ph.net.swak.pamobile.di.module.sub.HomeActivityFragmentModule
import ph.net.swak.pamobile.ui.content.filing.itinerary.FileItineraryActivity
import ph.net.swak.pamobile.ui.content.home.HomeActivity
import ph.net.swak.pamobile.ui.content.login.LoginActivity

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [(HomeActivityFragmentModule::class)])
    abstract fun contributeHomeActivity(): HomeActivity

    @ContributesAndroidInjector
    abstract fun contributeFileItineraryActivity(): FileItineraryActivity

}
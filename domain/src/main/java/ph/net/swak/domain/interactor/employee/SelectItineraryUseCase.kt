package ph.net.swak.domain.interactor.employee

import io.reactivex.Flowable
import ph.net.swak.domain.interactor.type.FlowableUseCaseWithParameter
import ph.net.swak.domain.model.Itinerary
import ph.net.swak.domain.repository.EmployeeRepository
import ph.net.swak.domain.util.Resource

class SelectItineraryUseCase(private val repo: EmployeeRepository) :
        FlowableUseCaseWithParameter<Int, List<Itinerary>> {

    override fun execute(parameter: Int): Flowable<List<Itinerary>> = repo.selectItinerary(parameter)

}
package ph.net.swak.data.mapper

import ph.net.swak.data.GetItineraryQuery
import ph.net.swak.data.db.entities.ItineraryEntity
import ph.net.swak.data.mapper.base.ApiToDbMapper
import ph.net.swak.data.util.parseDate
import ph.net.swak.data.util.parseYear
import ph.net.swak.domain.model.Itinerary

class ItineraryMapper : ApiToDbMapper<List<GetItineraryQuery.Item>, List<ItineraryEntity>, List<Itinerary>> {

    override fun apiToDb(apiModel: List<GetItineraryQuery.Item>): List<ItineraryEntity> =
            apiModel.map {
                ItineraryEntity(
                        it.id().toLong(),
                        it.empId(),
                        it.isApproved,
                        parseDate(it.dateFiled()),
                        parseDate(it.dateCreated()),
                        it.reason(),
                        it.createdBy(),
                        parseYear(it.dateFrom()),
                        parseDate(it.dateFrom()),
                        parseDate(it.dateTo()),
                        it.disapprovedReason(),
                        it.appDisBy(),
                        parseDate(it.appDisDate())
                )
            }

    override fun dbToDomain(entity: List<ItineraryEntity>): List<Itinerary> =
            entity.map {
                Itinerary(
                        it.isApproved,
                        it.dateFiled,
                        it.dateCreated,
                        it.reason,
                        it.createdBy,
                        it.dateFrom,
                        it.dateTo,
                        it.disapprovedReason,
                        it.appDisBy,
                        it.appDisDate,
                        "Success"
                )
            }

}
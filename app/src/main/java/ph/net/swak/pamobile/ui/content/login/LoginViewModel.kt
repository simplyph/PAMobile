package ph.net.swak.pamobile.ui.content.login

import android.arch.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import ph.net.swak.domain.interactor.employee.GetLoginUseCase
import ph.net.swak.domain.model.Login
import ph.net.swak.domain.model.LoginCredential
import ph.net.swak.domain.util.Resource
import ph.net.swak.domain.util.Status
import ph.net.swak.pamobile.ui.base.BaseViewModel
import javax.inject.Inject

internal class LoginViewModel @Inject constructor(
        private val getApiLoginUseCase: GetLoginUseCase
) : BaseViewModel() {

    val login = MutableLiveData<Resource<Login>>().apply { value = Resource(Status.IDLE, "", null) }

    fun checkLogin(user: String, pass: String) {
        addDisposable(getApiLoginUseCase
                .execute(LoginCredential(user, pass))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    login.apply { value = Resource(Status.LOADING, "", null) }
                }
                .subscribeBy(
                        onSuccess = {
                            login.apply { value = Resource(Status.SUCCESS, "", it) }
                        },
                        onError = {
                            login.apply { value = Resource(Status.ERROR, it.message, null) }
                        }
                )
        )
    }
}
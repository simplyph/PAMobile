package ph.net.swak.pamobile.ui.base

import android.support.v4.app.Fragment
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import ph.net.swak.pamobile.di.Injectable
import javax.inject.Inject

abstract class BaseFragment: Fragment(), Injectable {

    @Inject lateinit var viewModelFactory: PAMobileViewModelFactory

    private val disposables by lazy {
        CompositeDisposable()
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    protected fun addDisposable(disposable: Disposable) {
        if (!disposables.isDisposed)
            disposables.add(disposable)
    }

    protected fun addDisposables(vararg disposable: Disposable) {
        disposable.forEach { d ->
            if(!d.isDisposed)
                disposables.add(d)
        }
    }



}
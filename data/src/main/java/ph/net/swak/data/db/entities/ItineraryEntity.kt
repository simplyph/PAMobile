package ph.net.swak.data.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.util.*

@Entity(tableName = "itineraries")
data class ItineraryEntity(
        @PrimaryKey
        val id: Long,

        @ColumnInfo(name = "emp_id")
        val empId: Int,

        @ColumnInfo(name = "is_approved")
        val isApproved: Int,

        @ColumnInfo(name = "date_filed")
        val dateFiled: Date,

        @ColumnInfo(name = "date_created")
        val dateCreated: Date,

        @ColumnInfo(name = "reason")
        val reason: String,

        @ColumnInfo(name = "created_by")
        val createdBy: String,

        @ColumnInfo(name = "year_covering")
        val coveringYear: Int,

        @ColumnInfo(name = "date_from")
        val dateFrom: Date,

        @ColumnInfo(name = "date_to")
        val dateTo: Date,

        @ColumnInfo(name = "reason_disapproved")
        val disapprovedReason: String,

        @ColumnInfo(name = "app_dis_by")
        val appDisBy: String,

        @ColumnInfo(name = "date_app_dis")
        val appDisDate: Date
)
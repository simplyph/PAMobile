package ph.net.swak.pamobile.ui.content.filing.leave


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.support.v4.widget.refreshes
import com.jakewharton.rxbinding2.support.v4.widget.refreshing
import com.jakewharton.rxbinding2.view.clicks
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_leave.*
import org.jetbrains.anko.support.v4.intentFor
import ph.net.swak.domain.util.Resource
import ph.net.swak.domain.util.Status
import ph.net.swak.pamobile.R
import ph.net.swak.pamobile.ui.base.BaseFragment
import ph.net.swak.pamobile.ui.content.filing.itinerary.FileItineraryActivity
import ph.net.swak.pamobile.ui.content.home.HomeActivity
import javax.inject.Inject

class LeaveFragment : BaseFragment() {

    @Inject lateinit var sharedPrefs: SharedPreferences
    private lateinit var viewModel: LeaveViewModel
    private lateinit var activity: HomeActivity
    private lateinit var fastAdapter: FastItemAdapter<LeaveItem>
    private val empId by lazy {
        sharedPrefs.getString("username", "0").toInt()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LeaveViewModel::class.java)
        viewModel.empIdTrigger.postValue(Resource(Status.SUCCESS, "", empId))
        viewModel.leaves.observe(this, Observer {
            it?.let {
                fastAdapter.setNewList(it.map {
                    LeaveItem(
                            it.isApproved,
                            it.appDisBy,
                            it.dateFiled,
                            it.dateLeave,
                            it.daysFiled,
                            it.daysApproved,
                            it.isPaid,
                            it.leaveName,
                            it.reason
                    )
                })
            }
        })
        viewModel.empIdTrigger.observe(this, Observer {
            it?.let {
                when (it.status) {
                    Status.SUCCESS -> swipe_refresh.refreshing().accept(false)
                    Status.LOADING -> swipe_refresh.refreshing().accept(true)
                    Status.ERROR -> swipe_refresh.refreshing().accept(false)
                    else -> {

                    }
                }
            }
        })

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_leave, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity = getActivity() as HomeActivity

        fastAdapter = FastItemAdapter()
        item_list.adapter = fastAdapter
        item_list.itemAnimator = DefaultItemAnimator()
        item_list.layoutManager = LinearLayoutManager(context)
        item_list.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))

        setupControls()
    }

    private fun setupControls() {
        addDisposables(
                activity.fab.clicks().subscribe {
                    val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity,
                            activity.fab,
                            ViewCompat.getTransitionName(activity.fab))
                    startActivity(intentFor<FileItineraryActivity>(), options.toBundle())
                },
                swipe_refresh.refreshes().subscribe { viewModel.fetchLeaves(empId) }
        )
    }

}

package ph.net.swak.pamobile.util

import android.annotation.SuppressLint
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.TextView
import ph.net.swak.pamobile.R

@SuppressLint("RestrictedApi")
fun BottomNavigationView.modify() {
    //
    this.itemTextColor = ContextCompat.getColorStateList(context, R.color.md_red_500)
    this.itemIconTintList = ContextCompat.getColorStateList(context, R.color.md_red_500)

    //
    val menuView = this.getChildAt(0) as BottomNavigationMenuView
    val shiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode")
    shiftingMode.isAccessible = true
    shiftingMode.setBoolean(menuView, false)
    shiftingMode.isAccessible = false

    try {
        for (i in 0 until menuView.childCount) {
            val item = menuView.getChildAt(i) as BottomNavigationItemView
            item.findViewById<TextView>(R.id.smallLabel).textSize = 10.0f
            item.findViewById<TextView>(R.id.largeLabel).textSize = 10.0f
            item.setShiftingMode(false)
            item.setChecked(item.itemData.isChecked)
        }
    } catch (e: NoSuchFieldException) {
        Log.e("BNVHelper", "Unable to get shift mode field", e)
    } catch (e: IllegalAccessException) {
        Log.e("BNVHelper", "Unable to change value of shift mode", e)
    }
}
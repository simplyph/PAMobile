package ph.net.swak.data.mapper.base

interface ApiToDbMapper<in ApiType, DbType, out DomainType> {

    fun apiToDb(apiModel: ApiType): DbType

    fun dbToDomain(entity: DbType): DomainType

}
package ph.net.swak.pamobile.ui.content.filing.itinerary

import android.annotation.TargetApi
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.transition.Transition
import android.transition.TransitionInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import com.jakewharton.rxbinding2.widget.text
import kotlinx.android.synthetic.main.activity_file_itinerary.*
import ph.net.swak.pamobile.R
import ph.net.swak.pamobile.operators.pressesUp
import ph.net.swak.pamobile.ui.animation.transition.OnRevealAnimationListener
import ph.net.swak.pamobile.ui.animation.transition.animateRevealShow
import ph.net.swak.pamobile.ui.base.BaseActivity
import ph.net.swak.pamobile.ui.base.PAMobileViewModelFactory
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class FileItineraryActivity : BaseActivity() {

    @Inject lateinit var viewModelFactory: PAMobileViewModelFactory

    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ItineraryViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_file_itinerary)
        setSupportActionBar(toolbar)
        supportActionBar?.setTitle(R.string.title_file_itinerary)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportPostponeEnterTransition()

        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        //    setupEnterAnimation()
        //    setupExitAnimation()
        //} else {
        initViews()
        //}
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupEnterAnimation() {
        val transition = TransitionInflater.from(this).inflateTransition(R.transition.changebounds_with_arcmotion)
        window.sharedElementEnterTransition = transition
        transition.addListener(object : Transition.TransitionListener {
            override fun onTransitionEnd(transition: Transition?) {
                transition?.removeListener(this)
                initViews()
                //animateRevealShow(this@FileItineraryActivity.coordinator)
            }

            override fun onTransitionResume(transition: Transition?) {
            }

            override fun onTransitionPause(transition: Transition?) {
            }

            override fun onTransitionCancel(transition: Transition?) {
            }

            override fun onTransitionStart(transition: Transition?) {
            }
        })
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupExitAnimation() {
        //val fade = Fade()
        //window.returnTransition = fade
        //fade.duration = resources.getInteger(R.integer.animation_duration).toLong()
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private fun animateRevealShow(view: View) {
        val cx: Int = view.left
        val cy: Int = view.bottom
        coordinator.animateRevealShow(this, appbar.width, R.color.colorAccent,
                cx, cy, object : OnRevealAnimationListener {
            override fun onRevealShow() {
                initViews()
            }

            override fun onRevealHide() {
            }
        })
    }

    private fun initViews() {
        Handler(Looper.getMainLooper()).post {
            val animation = AnimationUtils.loadAnimation(this, android.R.anim.fade_in)
            animation.duration = 300
            file_form.startAnimation(animation)
            file_form.visibility = View.VISIBLE
            supportStartPostponedEnterTransition()
            setupControls()
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBackPressed() {
        super.onBackPressed()
        finishAfterTransition()
        //coordinator.animateRevealHide(this, R.color.colorAccent, appbar.width / 2, object : OnRevealAnimationListener {
        //    override fun onRevealShow() {
        //    }
//
        //    override fun onRevealHide() {
        //        finishAfterTransition()
        //    }
        //})
    }

    private fun backPressed() {
        super.onBackPressed()
    }

    private fun setupControls() {
        val cal: Calendar = Calendar.getInstance()
        val dateFormat = "MMM d, yyyy"
        val timeFormat = "h:mm a"
        val sdfDate = SimpleDateFormat(dateFormat, Locale.US)
        val sdfTime = SimpleDateFormat(timeFormat, Locale.US)

        addDisposables(
                itinerary_date.pressesUp()
                        .subscribe {
                            DatePickerDialog(this@FileItineraryActivity,
                                    DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                                        cal.set(Calendar.YEAR, year)
                                        cal.set(Calendar.MONTH, month)
                                        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                                        itinerary_date.text().accept(sdfDate.format(cal.time))
                                    }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
                                    .show()

                        },
                itinerary_start_time.pressesUp()
                        .subscribe {
                            TimePickerDialog(this@FileItineraryActivity,
                                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                                        cal.set(Calendar.HOUR, hourOfDay)
                                        cal.set(Calendar.MINUTE, minute)
                                        itinerary_start_time.text().accept(sdfTime.format(cal.time))
                                    }, cal.get(Calendar.HOUR), cal.get(Calendar.MINUTE), false)
                                    .show()
                        },
                itinerary_end_time.pressesUp()
                        .subscribe {
                            TimePickerDialog(this@FileItineraryActivity,
                                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                                        cal.set(Calendar.HOUR, hourOfDay)
                                        cal.set(Calendar.MINUTE, minute)
                                        itinerary_end_time.text().accept(sdfTime.format(cal.time))
                                    }, cal.get(Calendar.HOUR), cal.get(Calendar.MINUTE), false)
                                    .show()
                        }
        )

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_filing, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when (it.itemId) {
                android.R.id.home -> {
                    onBackPressed()
                }

                R.id.navigation_file -> {
                    validateAndCreateResult()
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun validateAndCreateResult() {
        if (itinerary_date.text.isBlank()) {
            itinerary_date.error = getString(R.string.error_field_required)
            itinerary_date.requestFocus()
            return
        }

        if (itinerary_start_time.text.isBlank()) {
            itinerary_start_time.error = getString(R.string.error_field_required)
            itinerary_start_time.requestFocus()
            return
        }

        if (itinerary_end_time.text.isBlank()) {
            itinerary_end_time.error = getString(R.string.error_field_required)
            itinerary_end_time.requestFocus()
            return
        }

        if (itinerary_reason.text.isBlank()) {
            itinerary_reason.error = getString(R.string.error_field_required)
            itinerary_reason.requestFocus()
            return
        }

        setResult(Activity.RESULT_OK, createResult())
        onBackPressed()
    }

    private fun createResult(): Intent {
        val data = Intent()
        data.putExtra("dateKey", itinerary_date.text.toString())
        data.putExtra("startTimeKey", itinerary_start_time.text.toString())
        data.putExtra("endTimeKey", itinerary_end_time.text.toString())
        data.putExtra("reasonKey", itinerary_reason.text.toString())
        return data
    }
}

package ph.net.swak.pamobile

import android.app.Activity
import android.app.Application
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import ph.net.swak.pamobile.di.AppInjector
import timber.log.Timber
import javax.inject.Inject

class PAMobileApplication : Application(), HasActivityInjector {

    @Inject lateinit var androidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        // Timber
        // TODO: Create implementation of writing logs into external file or viewable source
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())

        AppInjector.initInjector(this)

    }

    override fun activityInjector() = androidInjector
}

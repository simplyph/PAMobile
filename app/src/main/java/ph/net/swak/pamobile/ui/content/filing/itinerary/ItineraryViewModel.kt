package ph.net.swak.pamobile.ui.content.filing.itinerary

import android.arch.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import ph.net.swak.domain.interactor.employee.GetItineraryUseCase
import ph.net.swak.domain.interactor.employee.PostItineraryUseCase
import ph.net.swak.domain.interactor.employee.SelectItineraryUseCase
import ph.net.swak.domain.model.FileItinerary
import ph.net.swak.domain.util.Resource
import ph.net.swak.domain.util.Status
import ph.net.swak.pamobile.ui.base.BaseViewModel
import ph.net.swak.pamobile.util.switchMap
import ph.net.swak.pamobile.util.toLiveData
import javax.inject.Inject

internal class ItineraryViewModel @Inject constructor(
        private val selectItinerary: SelectItineraryUseCase,
        private val postItinerary: PostItineraryUseCase,
        private val getItinerary: GetItineraryUseCase
) : BaseViewModel() {

    val empIdTrigger = MutableLiveData<Resource<Int>>()
            .apply { value = Resource(Status.IDLE, "", 0) }

    val itineraries = empIdTrigger.switchMap {
        selectItinerary.execute(it.data ?: 0)
                .observeOn(AndroidSchedulers.mainThread())
                .toLiveData()
    }

    fun fetchItineraries(id: Int) {
        addDisposable(getItinerary.execute(id)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    empIdTrigger.apply { value = Resource(Status.LOADING, "", id) }
                }
                .doOnError {
                    empIdTrigger.apply { value = Resource(Status.ERROR, "", id) }
                }
                .doOnComplete {
                    empIdTrigger.apply { value = Resource(Status.SUCCESS, "", id) }
                }
                .subscribe()
        )
    }


    fun fileItinerary(fileItinerary: FileItinerary) {
        addDisposable(postItinerary.execute(fileItinerary)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        )
    }
}
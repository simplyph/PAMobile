package ph.net.swak.pamobile.util

import android.os.Handler
import android.os.Looper
import ph.net.swak.pamobile.di.annotation.scope.ApplicationScope
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Inject

@ApplicationScope
class AppExecutors(val diskIO: Executor, private val networkIO: Executor, private val mainIO: Executor) {

    @Inject
    constructor() : this(Executors.newSingleThreadExecutor(), Executors.newFixedThreadPool(3), MainIOExecutor())

    companion object {
        private class MainIOExecutor : Executor {
            override fun execute(command: Runnable) {
                Handler(Looper.getMainLooper()).post(command)
            }
        }
    }
}
package ph.net.swak.pamobile.operators

import android.view.MotionEvent
import com.jakewharton.rxbinding2.view.touches

inline fun android.view.View.pressesUp() = touches().filter { it.action == MotionEvent.ACTION_UP }
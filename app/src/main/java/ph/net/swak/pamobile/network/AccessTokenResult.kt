package ph.net.swak.pamobile.network

import com.google.gson.annotations.SerializedName

data class AccessTokenResult(
        @SerializedName("access_token")
        var accessToken: String,
        @SerializedName("token_type")
        var tokenType: String,
        @SerializedName("expires_in")
        var expiresIn: String,
        @SerializedName("as:client_id")
        var clientId: String,
        @SerializedName("userName")
        var userName: String,
        @SerializedName(".issued")
        var issued: String,
        @SerializedName(".expires")
        var expires: String
)
package ph.net.swak.pamobile.ui.content.filing.leave

import android.arch.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import ph.net.swak.domain.interactor.employee.GetLeaveUseCase
import ph.net.swak.domain.interactor.employee.SelectLeaveUseCase
import ph.net.swak.domain.util.Resource
import ph.net.swak.domain.util.Status
import ph.net.swak.pamobile.ui.base.BaseViewModel
import ph.net.swak.pamobile.util.switchMap
import ph.net.swak.pamobile.util.toLiveData
import javax.inject.Inject

internal class LeaveViewModel @Inject constructor(
        private val selectLeave: SelectLeaveUseCase,
        private val getLeave: GetLeaveUseCase
) : BaseViewModel() {

    val empIdTrigger = MutableLiveData<Resource<Int>>()
            .apply { value = Resource(Status.IDLE, "", 0) }

    val leaves = empIdTrigger.switchMap {
        selectLeave.execute(it.data ?: 0)
                .observeOn(AndroidSchedulers.mainThread())
                .toLiveData()
    }

    fun fetchLeaves(id: Int) {
        addDisposable(getLeave.execute(id)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    empIdTrigger.apply { value = Resource(Status.LOADING, "", id) }
                }
                .doOnError {
                    empIdTrigger.apply { value = Resource(Status.ERROR, "", id) }
                }
                .doOnComplete {
                    empIdTrigger.apply { value = Resource(Status.SUCCESS, "", id) }
                }
                .subscribe()
        )
    }

}
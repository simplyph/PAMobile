package ph.net.swak.domain.interactor.employee

import io.reactivex.Single
import ph.net.swak.domain.model.Employee
import ph.net.swak.domain.repository.EmployeeRepository
import ph.net.swak.domain.interactor.type.SingleUseCaseWithParameter

class GetEmployeesUseCase(private val repo: EmployeeRepository) : SingleUseCaseWithParameter<Int, List<Employee>> {

    override fun execute(parameter: Int): Single<List<Employee>> = repo.getEmployees()

}
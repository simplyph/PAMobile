package ph.net.swak.pamobile.util

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.LiveDataReactiveStreams
import android.arch.lifecycle.Transformations
import org.reactivestreams.Publisher

fun <T> Publisher<T>.toLiveData() = LiveDataReactiveStreams.fromPublisher(this)

fun <T, R> LiveData<T>.switchMap(bar: (T) -> LiveData<R>): LiveData<R> = Transformations.switchMap(this, bar)
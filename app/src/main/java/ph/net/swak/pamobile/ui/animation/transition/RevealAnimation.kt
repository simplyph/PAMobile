package ph.net.swak.pamobile.ui.animation.transition

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.os.Build
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.ViewAnimationUtils
import android.support.annotation.ColorRes
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.view.View
import ph.net.swak.pamobile.R
import ph.net.swak.pamobile.ui.animation.transition.OnRevealAnimationListener


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun View.animateRevealHide(ctx: Context, @ColorRes color: Int, finalRadius: Int, listener: OnRevealAnimationListener) {
    val cx = this.left
    val cy = this.bottom
    val initialRadius = this.width

    val anim = ViewAnimationUtils.createCircularReveal(this, cx, cy, initialRadius.toFloat(), finalRadius.toFloat())
    //anim.interpolator = AccelerateDecelerateInterpolator()
    //anim.interpolator = FastOutLinearInInterpolator()
    anim.addListener(object : AnimatorListenerAdapter() {
        override fun onAnimationStart(animation: Animator) {
            super.onAnimationStart(animation)
            this@animateRevealHide.setBackgroundColor(ContextCompat.getColor(ctx, color))
            listener.onRevealHide()
        }

        override fun onAnimationEnd(animation: Animator) {
            super.onAnimationEnd(animation)
            this@animateRevealHide.visibility = View.INVISIBLE
        }
    })
    anim.duration = ctx.resources.getInteger(R.integer.animation_duration).toLong()
    anim.start()
}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun View.animateRevealShow(ctx: Context, startRadius: Int,
                           @ColorRes color: Int, x: Int, y: Int, listener: OnRevealAnimationListener) {
    val finalRadius = Math.hypot(this.width.toDouble(), this.height.toDouble()).toFloat()

    val anim = ViewAnimationUtils.createCircularReveal(this, x, y, startRadius.toFloat(), finalRadius)
    val anim2 = ViewAnimationUtils.createCircularReveal(this, x, y, startRadius.toFloat(), finalRadius)
    anim.duration = ctx.resources.getInteger(R.integer.animation_duration).toLong()
    anim.startDelay = 100
    anim.interpolator = AccelerateDecelerateInterpolator()
    anim.addListener(object : AnimatorListenerAdapter() {
        override fun onAnimationStart(animation: Animator) {
            this@animateRevealShow.setBackgroundColor(ContextCompat.getColor(ctx, color))
        }

        override fun onAnimationEnd(animation: Animator) {
            anim2.start()
        }
    })

    anim2.duration = ctx.resources.getInteger(R.integer.animation_duration).toLong()
    anim2.startDelay = 20
    anim2.interpolator = AccelerateDecelerateInterpolator()
    anim2.addListener(object : AnimatorListenerAdapter() {
        override fun onAnimationStart(animation: Animator) {
            this@animateRevealShow.setBackgroundColor(ContextCompat.getColor(ctx, R.color.md_white_1000))
        }

        override fun onAnimationEnd(animation: Animator) {
            this@animateRevealShow.visibility = View.VISIBLE
            listener.onRevealShow()
        }
    })
    anim.start()
}
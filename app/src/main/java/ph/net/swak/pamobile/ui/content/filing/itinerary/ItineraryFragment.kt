package ph.net.swak.pamobile.ui.content.filing.itinerary


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.support.v4.widget.refreshes
import com.jakewharton.rxbinding2.support.v4.widget.refreshing
import com.jakewharton.rxbinding2.view.clicks
import com.mikepenz.fastadapter.commons.adapters.FastItemAdapter
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.fragment_itinerary.*
import org.jetbrains.anko.support.v4.intentFor
import ph.net.swak.domain.util.Resource
import ph.net.swak.domain.util.Status
import ph.net.swak.pamobile.R
import ph.net.swak.pamobile.ui.base.BaseFragment
import ph.net.swak.pamobile.ui.content.home.HomeActivity
import javax.inject.Inject

class ItineraryFragment : BaseFragment() {

    @Inject lateinit var sharedPrefs: SharedPreferences
    private lateinit var viewModel: ItineraryViewModel
    private lateinit var activity: HomeActivity
    private lateinit var fastAdapter: FastItemAdapter<ItineraryItem>
    private val empId by lazy {
        sharedPrefs.getString("username", "0").toInt()
    }

    private var sampleewan = "No data"

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ItineraryViewModel::class.java)
        viewModel.empIdTrigger.postValue(Resource(Status.SUCCESS, "", empId))
        viewModel.itineraries.observe(this, Observer {
            it?.let {
                fastAdapter.setNewList(
                        it.map {
                            ItineraryItem(
                                    it.isApproved,
                                    it.appDisBy,
                                    it.dateFiled,
                                    it.dateFrom,
                                    it.dateTo,
                                    it.reason
                            )
                        }
                )
            }
        })
        viewModel.empIdTrigger.observe(this, Observer {
            it?.let {
                when (it.status) {
                    Status.SUCCESS -> swipe_refresh.refreshing().accept(false)
                    Status.LOADING -> swipe_refresh.refreshing().accept(true)
                    Status.ERROR -> swipe_refresh.refreshing().accept(false)
                    else -> {

                    }
                }
            }
        })

        //viewModel.isFiling.observe(this, Observer {
        //    it?.let {
        //        //if (it) {
        //        //    fileItinerary.text = "Filing ItineraryEntity..."
        //        //} else {
        //        //    fileItinerary.text = "File ItineraryEntity"
        //        //    viewModel.isSuccess.observe(this, Observer {
        //        //        it?.let {
        //        //            if (it)
        //        //                Toast.makeText(activity, "Succeded on filing the itinerary.", Toast.LENGTH_SHORT).show()
        //        //            else
        //        //                Toast.makeText(activity, "Failed filing the itinerary.", Toast.LENGTH_SHORT).show()
        //        //        }
        //        //    })
        //        //}
        //    }
        //})
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_itinerary, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity = getActivity() as HomeActivity

        fastAdapter = FastItemAdapter()
        item_list.adapter = fastAdapter
        item_list.itemAnimator = DefaultItemAnimator()
        item_list.layoutManager = LinearLayoutManager(context)
        item_list.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
//
        //fileItinerary.clicks()
        //        .observeOn(AndroidSchedulers.mainThread())
        //        .subscribe {
        //            viewModel.fileItinerary(
        //                    FileItinerary(
        //                            17001020,
        //                            2017,
        //                            "2017-11-18 10:00:00",
        //                            "2017-11-18 19:00:00",
        //                            "No time out due to brownout"
        //                    )
        //            )
        //        }
//

        setupControls()
    }

    private fun setupControls() {
        addDisposables(
                activity.fab.clicks().subscribe {
                    val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity,
                            activity.fab,
                            ViewCompat.getTransitionName(activity.fab))
                    parentFragment?.startActivityForResult(intentFor<FileItineraryActivity>(), 1, options.toBundle())

                    parentFragment?.setTargetFragment(this, 1)

                },
                swipe_refresh.refreshes().subscribe { viewModel.fetchItineraries(empId) }
        )
    }
}

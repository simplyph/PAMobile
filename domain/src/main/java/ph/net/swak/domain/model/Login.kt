package ph.net.swak.domain.model

data class Login(
        val userId: Int,
        val employeeName: String,
        val accountImage: String,
        val accountName: String,
        val accountType: Int,
        val designation: String,
        val employment: String,
        val isValid: Boolean,
        val isActive: Boolean,
        val isDivisionHead: Boolean,
        val isDepartmentHead: Boolean,
        val isEvaluator: Boolean
)
package ph.net.swak.pamobile.di.module.sub

import android.app.Application
import android.arch.persistence.room.Room
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import ph.net.swak.data.db.PAMobileDatabase
import ph.net.swak.data.db.dao.ItineraryDao
import ph.net.swak.data.db.dao.LeaveDao
import ph.net.swak.data.repository.EmployeeRepositoryImpl
import ph.net.swak.data.service.IEmployeeService
import ph.net.swak.domain.repository.EmployeeRepository
import ph.net.swak.pamobile.di.annotation.scope.ApplicationScope
import javax.inject.Named

@Module
class DataModule {

    @Provides
    @ApplicationScope
    fun provideEmployeeRepository(
            @Named(ThreadingModule.BACKGROUND_SCHEDULER)
            backgroundScheduler: Scheduler,
            employeeService: IEmployeeService,
            itineraryDao: ItineraryDao,
            leaveDao: LeaveDao
    ): EmployeeRepository = EmployeeRepositoryImpl(backgroundScheduler, employeeService, itineraryDao, leaveDao)

    @Provides
    @ApplicationScope
    fun providePAMobileDatabase(application: Application): PAMobileDatabase =
            Room.databaseBuilder(application, PAMobileDatabase::class.java, "pa_mobile.db")
                    .fallbackToDestructiveMigration()
                    .build()

    @Provides
    @ApplicationScope
    fun provideItineraryDao(db: PAMobileDatabase): ItineraryDao = db.itineraryDao()

    @Provides
    @ApplicationScope
    fun provideLeaveDao(db: PAMobileDatabase): LeaveDao = db.leaveDao()

}





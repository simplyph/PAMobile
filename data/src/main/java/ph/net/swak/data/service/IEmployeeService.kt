package ph.net.swak.data.service

import io.reactivex.Completable
import io.reactivex.Single
import ph.net.swak.domain.model.*

interface IEmployeeService {

    fun fetchEmployees(): Single<List<Employee>>

    fun fetchItinerary(id: Int): Single<List<Itinerary>>

    fun postItinerary(fileItinerary: FileItinerary): Completable

    fun getItinerary(id: Int): Completable

    fun getLogin(credential: LoginCredential): Single<Login>

    fun getLeave(id: Int): Completable
}
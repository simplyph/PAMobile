package ph.net.swak.pamobile.network

import android.content.Context
import com.google.gson.Gson
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import ph.net.swak.pamobile.util.*
import java.io.BufferedReader
import java.io.DataOutputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class TokenAuthenticator(private val context: Context) : Authenticator {

    lateinit var accessTokenResult: AccessTokenResult

    override fun authenticate(route: Route?, response: Response?): Request? {
        // TODO: Change with sharedPreference
        val username = "knowell"
        val password = "knowell10"
        val endpoint = "$BASE_URL$TOKEN_ENDPOINT"

        val tokenResult = accessToken(endpoint, username, password)

        return when(tokenResult) {
            true -> response?.request()?.newBuilder()?.header("Authorization", "Bearer ${accessTokenResult.accessToken}")?.build()
            false -> null
        }
    }

    private fun accessToken(endpoint: String, username: String, password: String): Boolean {
        val refreshUrl = URL(endpoint)
        val urlConnection = refreshUrl.openConnection() as HttpURLConnection
        urlConnection.doInput = true
        urlConnection.requestMethod = "POST"
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
        urlConnection.useCaches = false

        val urlParam = "grant_type=password&client_type=$CLIENT_TYPE&client_id=$CLIENT_ID&client_secret=$CLIENT_SECRET&username=$username&password=$password"

        urlConnection.doOutput = true
        val wr = DataOutputStream(urlConnection.outputStream)
        wr.writeBytes(urlParam)
        wr.flush()
        wr.close()

        val responseCode: Int = urlConnection.responseCode

        return when (responseCode) {
            200 -> {
                val bufferedReader = BufferedReader(InputStreamReader(urlConnection.inputStream))
                val response = StringBuffer()

                while (true) {
                    val inputLine = bufferedReader.readLine() ?: break
                    response.append(inputLine)
                }

                bufferedReader.close()
                accessTokenResult = Gson().fromJson(response.toString(), AccessTokenResult::class.java)

                true
            }
            else -> false
        }
    }


}
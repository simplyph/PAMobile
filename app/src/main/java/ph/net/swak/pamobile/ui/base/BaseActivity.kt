package ph.net.swak.pamobile.ui.base

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    private val disposables by lazy {
        CompositeDisposable()
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    protected fun addDisposable(disposable: Disposable) {
        if (!disposables.isDisposed)
            disposables.add(disposable)
    }

    protected fun addDisposables(vararg disposable: Disposable) {
        disposable.forEach { d ->
            if(!d.isDisposed)
                disposables.add(d)
        }
    }

}
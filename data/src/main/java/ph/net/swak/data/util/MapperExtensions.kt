package ph.net.swak.data.util

import ph.net.swak.data.GetItineraryQuery
import ph.net.swak.data.GetLeaveQuery
import ph.net.swak.data.GetLoginQuery
import ph.net.swak.data.db.entities.ItineraryEntity
import ph.net.swak.data.db.entities.LeaveEntity
import ph.net.swak.data.mapper.ItineraryMapper
import ph.net.swak.data.mapper.LeaveMapper
import ph.net.swak.data.mapper.LoginMapper
import ph.net.swak.domain.model.Itinerary
import ph.net.swak.domain.model.Leave
import ph.net.swak.domain.model.Login

fun GetLoginQuery.Login.toDomain(): Login = LoginMapper().dbToDomain(LoginMapper().apiToDb(this))

@JvmName(name = "ItineraryApiToDomain")
fun List<GetItineraryQuery.Item>.toDomain(): List<Itinerary> = ItineraryMapper().dbToDomain(ItineraryMapper().apiToDb(this))

@JvmName(name = "ItineraryDbToDomain")
fun List<ItineraryEntity>.toDomain(): List<Itinerary> = ItineraryMapper().dbToDomain(this)

@JvmName(name = "ItineraryApiToDb")
fun List<GetItineraryQuery.Item>.toDb(): List<ItineraryEntity> = ItineraryMapper().apiToDb(this)

@JvmName(name = "LeaveApiToDomain")
fun List<GetLeaveQuery.Item>.toDomain(): List<Leave> = LeaveMapper().dbToDomain(LeaveMapper().apiToDb(this))

@JvmName(name = "LeaveDbToDomain")
fun List<LeaveEntity>.toDomain(): List<Leave> = LeaveMapper().dbToDomain(this)

@JvmName(name = "LeaveApiToDb")
fun List<GetLeaveQuery.Item>.toDb(): List<LeaveEntity> = LeaveMapper().apiToDb(this)
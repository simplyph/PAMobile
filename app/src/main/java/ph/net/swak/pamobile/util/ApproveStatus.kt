package ph.net.swak.pamobile.util

enum class ApproveStatus {
    FOR_APPROVAL,
    APPROVED,
    DISAPPROVED,
    FILING;
}

fun ApproveStatusText(isApproved: Int) = when (isApproved) {
    0 -> "Disapproved"
    1 -> "Approved"
    2 -> "For Approval"
    9 -> "Filing"
    else -> "Unknown"
}
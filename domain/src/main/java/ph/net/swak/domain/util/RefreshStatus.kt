package ph.net.swak.domain.util

enum class RefreshStatus {
    IDLE,
    LOADING,
    SUCCESS,
    ERROR,
}
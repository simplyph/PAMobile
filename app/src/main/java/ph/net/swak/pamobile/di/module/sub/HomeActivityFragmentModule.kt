package ph.net.swak.pamobile.di.module.sub

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ph.net.swak.pamobile.ui.content.filing.FilingsFragment
import ph.net.swak.pamobile.ui.content.filing.itinerary.ItineraryFragment
import ph.net.swak.pamobile.ui.content.filing.leave.LeaveFragment

@Module
abstract class HomeActivityFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeItineraryFragment(): ItineraryFragment

    @ContributesAndroidInjector
    abstract fun contributeLeaveFragment(): LeaveFragment

    @ContributesAndroidInjector
    abstract fun contributeFilingFragment(): FilingsFragment

}
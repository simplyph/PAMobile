package ph.net.swak.data.repository

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.Single
import ph.net.swak.data.db.dao.ItineraryDao
import ph.net.swak.data.db.dao.LeaveDao
import ph.net.swak.data.mapper.ItineraryMapper
import ph.net.swak.data.service.IEmployeeService
import ph.net.swak.data.util.toDomain
import ph.net.swak.domain.model.*
import ph.net.swak.domain.repository.EmployeeRepository

class EmployeeRepositoryImpl(
        private val scheduler: Scheduler,
        private val employeeService: IEmployeeService,
        private val itineraryDao: ItineraryDao,
        private val leaveDao: LeaveDao
) : EmployeeRepository {

    override fun getEmployees(): Single<List<Employee>> = Single
            .defer { employeeService.fetchEmployees() }
            .subscribeOn(scheduler)

    override fun selectItinerary(id: Int): Flowable<List<Itinerary>> = Flowable
            .defer { itineraryDao.getItinerary(id).map { it.toDomain() } }
            .subscribeOn(scheduler)

    override fun selectLeave(id: Int): Flowable<List<Leave>> = Flowable
            .defer { leaveDao.getLeave(id).map { it.toDomain() } }
            .subscribeOn(scheduler)

    override fun postItinerary(fileItinerary: FileItinerary): Completable = Completable
            .defer { employeeService.postItinerary(fileItinerary) }
            .subscribeOn(scheduler)

    override fun getItinerary(id: Int): Completable = Completable
            .defer { employeeService.getItinerary(id) }
            .subscribeOn(scheduler)

    override fun getLeave(id: Int): Completable = Completable
            .defer { employeeService.getLeave(id) }
            .subscribeOn(scheduler)

    override fun getLogin(credential: LoginCredential): Single<Login> = Single
            .defer { employeeService.getLogin(credential) }
            .subscribeOn(scheduler)

}
package ph.net.swak.domain.util

fun String.toTitleCase(): String {
    val DELIMITERS = " '-/"
    val sb = StringBuilder()
    var capNext = true

    this.forEach {
        sb.append(
                if (capNext)
                    it.toUpperCase()
                else
                    it.toLowerCase()

        )
        capNext = DELIMITERS.indexOf(it) >= 0
    }

    return sb.toString()
}
package ph.net.swak.pamobile.ui.base

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {

    private val disposables by lazy {
        CompositeDisposable()
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    protected fun addDisposable(disposable: Disposable) {
        if (!disposables.isDisposed)
            disposables.add(disposable)
    }

    protected fun addDisposables(vararg disposable: Disposable) {
        disposable.forEach { d ->
            if(!d.isDisposed)
                disposables.add(d)
        }

    }

}
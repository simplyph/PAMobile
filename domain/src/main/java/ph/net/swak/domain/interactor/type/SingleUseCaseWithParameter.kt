package ph.net.swak.domain.interactor.type

import io.reactivex.Single


interface SingleUseCaseWithParameter<in P, R> {

    fun execute(parameter: P): Single<R>

}
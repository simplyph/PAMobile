package ph.net.swak.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import ph.net.swak.data.db.converter.DateConverter
import ph.net.swak.data.db.dao.ItineraryDao
import ph.net.swak.data.db.dao.LeaveDao
import ph.net.swak.data.db.entities.ItineraryEntity
import ph.net.swak.data.db.entities.LeaveEntity

@Database(entities = [(ItineraryEntity::class), (LeaveEntity::class)], version = 2)
@TypeConverters(DateConverter::class)
abstract class PAMobileDatabase : RoomDatabase() {

    abstract fun itineraryDao(): ItineraryDao

    abstract fun leaveDao(): LeaveDao

}
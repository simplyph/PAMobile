package ph.net.swak.domain.interactor.type

import io.reactivex.Flowable

interface FlowableUseCaseWithParameter<in P, R> {

    fun execute(parameter: P): Flowable<R>

}
package ph.net.swak.pamobile.ui.content.filing.itinerary

import android.view.View
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.commons.utils.FastAdapterUIUtils
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.materialize.holder.StringHolder
import com.mikepenz.materialize.util.UIUtils
import kotlinx.android.synthetic.main.item_itinerary.view.*
import ph.net.swak.data.util.isCurrentDate
import ph.net.swak.data.util.to12HourTime
import ph.net.swak.data.util.toNumberDate
import ph.net.swak.data.util.toWholeDate
import ph.net.swak.pamobile.R
import ph.net.swak.pamobile.util.ApproveStatusText
import java.util.*

class ItineraryItem(
        private val status: Int,
        private val approval: String,
        private val dateFiled: Date,
        private val timeFrom: Date,
        private val timeTo: Date,
        private val reason: String
) : AbstractItem<ItineraryItem, ItineraryItem.ViewHolder>() {

    override fun getViewHolder(v: View) = ViewHolder(v)

    override fun getLayoutRes() = R.layout.item_itinerary

    override fun getType() = R.id.fast_adapter_list_itinerary_id

    inner class ViewHolder(private val view: View) : FastAdapter.ViewHolder<ItineraryItem>(view) {

        override fun bindView(item: ItineraryItem?, payloads: MutableList<Any>?) {
            item?.let {
                val ctx = view.context

                UIUtils.setBackground(view, FastAdapterUIUtils.getSelectableBackground(ctx))

                val statusText =
                        if(ApproveStatusText(it.status).toLowerCase() == "approved")
                            "${ApproveStatusText(it.status)} by ${it.approval}"
                        else
                            ApproveStatusText(it.status)
                val filedText = if (it.dateFiled.isCurrentDate()) "Today" else it.dateFiled.toNumberDate()

                StringHolder.applyTo(StringHolder(statusText), view.tv_status)
                StringHolder.applyTo(StringHolder(filedText), view.tv_filed)
                StringHolder.applyTo(StringHolder(it.timeFrom.toWholeDate()), view.tv_date)
                StringHolder.applyTo(StringHolder(it.timeFrom.to12HourTime()), view.tv_from)
                StringHolder.applyTo(StringHolder(it.timeTo.to12HourTime()), view.tv_to)
                StringHolder.applyTo(StringHolder(it.reason), view.tv_reason)
            }
        }

        override fun unbindView(item: ItineraryItem?) {
            view.tv_status.text = null
            view.tv_filed.text = null
            view.tv_date.text = null
            view.tv_from.text = null
            view.tv_to.text = null
            view.tv_reason.text = null
        }
    }
}
package ph.net.swak.pamobile.di.module.sub

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.android.ActivityKey
import dagger.multibindings.IntoMap
import ph.net.swak.pamobile.di.annotation.ViewModelKey
import ph.net.swak.pamobile.ui.content.filing.itinerary.ItineraryViewModel
import ph.net.swak.pamobile.ui.base.PAMobileViewModelFactory
import ph.net.swak.pamobile.ui.content.filing.leave.LeaveViewModel
import ph.net.swak.pamobile.ui.content.login.LoginViewModel

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class) internal
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ItineraryViewModel::class) internal
    abstract fun bindItineraryViewModel(viewModel: ItineraryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LeaveViewModel::class) internal
    abstract fun bindLeaveViewModel(viewModel: LeaveViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: PAMobileViewModelFactory): ViewModelProvider.Factory
}
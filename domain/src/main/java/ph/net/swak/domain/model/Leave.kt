package ph.net.swak.domain.model

import java.util.*

data class Leave (
        val isApproved: Int,
        val dateFiled: Date,
        val dateCreated: Date,
        val reason: String,
        val createdBy: String,
        val dateFrom: Date,
        val dateTo: Date,
        val disapprovedReason: String,
        val appDisBy: String,
        val dateAppDis: Date,
        val leaveName: String,
        val leaveId: Int,
        val leaveBalance: Float,
        val dateLeave: Date,
        val daysFiled: Float,
        val daysApproved: Float,
        val isPaid: Int,
        val leaveStatus: String,
        val returnMessage: String
)
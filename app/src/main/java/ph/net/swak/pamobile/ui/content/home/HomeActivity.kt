package ph.net.swak.pamobile.ui.content.home

import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_home.*
import org.jetbrains.anko.startActivity
import ph.net.swak.pamobile.R
import ph.net.swak.pamobile.ui.base.BaseActivity
import ph.net.swak.pamobile.ui.content.filing.FilingsFragment
import ph.net.swak.pamobile.ui.content.login.LoginActivity
import ph.net.swak.pamobile.util.modify
import javax.inject.Inject

class HomeActivity : BaseActivity() {

    @Inject lateinit var sharedPrefs: SharedPreferences

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_hris -> {
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_attendance -> {
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_payroll -> {
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_appraisal -> {
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)

        navigation.modify()
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        supportFragmentManager.beginTransaction().replace(container.id, FilingsFragment()).commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_profile, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when (it.itemId) {
                R.id.navigation_profile -> {
                }
                R.id.navigation_logout -> {
                    sharedPrefs.edit()
                            .clear()
                            .apply()

                    startActivity<LoginActivity>()
                    finish()
                }
                else -> {
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }
}

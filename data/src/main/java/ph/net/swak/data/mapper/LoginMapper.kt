package ph.net.swak.data.mapper

import ph.net.swak.data.GetLoginQuery
import ph.net.swak.data.mapper.base.ApiToDbMapper
import ph.net.swak.domain.model.Login

class LoginMapper : ApiToDbMapper<GetLoginQuery.Login, Login, Login> {

    override fun apiToDb(apiModel: GetLoginQuery.Login): Login =
            Login(
                    apiModel.userId(),
                    apiModel.employeeName(),
                    apiModel.accountImage(),
                    apiModel.accountName(),
                    apiModel.accountType(),
                    apiModel.designation(),
                    apiModel.employment(),
                    apiModel.isValid,
                    apiModel.isActive,
                    apiModel.isDivisionHead,
                    apiModel.isDepartmentHead,
                    apiModel.isEvaluator
            )

    override fun dbToDomain(entity: Login): Login = entity

}
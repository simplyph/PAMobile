package ph.net.swak.domain.repository

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import ph.net.swak.domain.model.*

interface EmployeeRepository {

    fun getEmployees(): Single<List<Employee>>

    fun selectItinerary(id: Int): Flowable<List<Itinerary>>

    fun selectLeave(id: Int): Flowable<List<Leave>>

    fun postItinerary(fileItinerary: FileItinerary): Completable

    fun getItinerary(id: Int): Completable

    fun getLogin(credential: LoginCredential): Single<Login>

    fun getLeave(id: Int): Completable

}
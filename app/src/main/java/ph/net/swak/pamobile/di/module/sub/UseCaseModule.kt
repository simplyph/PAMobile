package ph.net.swak.pamobile.di.module.sub

import dagger.Module
import dagger.Provides
import ph.net.swak.domain.interactor.employee.*
import ph.net.swak.domain.repository.EmployeeRepository
import ph.net.swak.pamobile.di.annotation.scope.ApplicationScope

@Module
class UseCaseModule {

    @Provides
    @ApplicationScope
    fun provideSelectItineraryUseCase(repository: EmployeeRepository): SelectItineraryUseCase =
            SelectItineraryUseCase(repository)

    @Provides
    @ApplicationScope
    fun provideSelectLeaveUseCase(repository: EmployeeRepository): SelectLeaveUseCase =
            SelectLeaveUseCase(repository)

    @Provides
    @ApplicationScope
    fun provideGetItineraryUseCase(repository: EmployeeRepository): GetItineraryUseCase =
            GetItineraryUseCase(repository)

    @Provides
    @ApplicationScope
    fun provideGetLeaveUseCase(repository: EmployeeRepository): GetLeaveUseCase =
            GetLeaveUseCase(repository)

    @Provides
    @ApplicationScope
    fun provideGetEmployeeUseCase(repository: EmployeeRepository): GetEmployeesUseCase =
            GetEmployeesUseCase(repository)

    @Provides
    @ApplicationScope
    fun providePostItineraryUseCase(repository: EmployeeRepository): PostItineraryUseCase =
            PostItineraryUseCase(repository)

    @Provides
    @ApplicationScope
    fun provideGetLoginUseCase(repository: EmployeeRepository): GetLoginUseCase =
            GetLoginUseCase(repository)

}
package ph.net.swak.pamobile.di.module.sub

import com.apollographql.apollo.ApolloClient
import dagger.Module
import dagger.Provides
import ph.net.swak.data.db.dao.ItineraryDao
import ph.net.swak.data.db.dao.LeaveDao
import ph.net.swak.data.service.EmployeeService
import ph.net.swak.data.service.IEmployeeService
import ph.net.swak.pamobile.di.annotation.scope.ApplicationScope

@Module
class ServiceModule {

    @Provides
    @ApplicationScope
    fun provideEmployeeService(apolloClient: ApolloClient, itineraryDao: ItineraryDao, leaveDao: LeaveDao):
            IEmployeeService = EmployeeService(apolloClient, itineraryDao, leaveDao)


}